
//@prepros-prepend states/Boot.js
//@prepros-prepend states/Preloader.js
//@prepros-prepend states/MainMenu.js
//@prepros-prepend states/Game.js
//@prepros-prepend states/EndMenu.js



var game = new Phaser.Game(320, 480, Phaser.CANVAS, 'game');
// var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, 'game');


game.state.add('Boot', PhaserGame.Boot);
game.state.add('Preloader', PhaserGame.Preloader);
game.state.add('MainMenu', PhaserGame.MainMenu);
game.state.add('Game', PhaserGame.Game);
game.state.add('EndMenu', PhaserGame.EndMenu);
// game.state.add('EndMenu', PhaserGame.EndMenu);

game.state.start("Boot");