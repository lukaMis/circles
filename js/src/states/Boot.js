console.log("Boot log");

var PhaserGame = PhaserGame || {};


PhaserGame.Boot = function (game) {};


PhaserGame.Boot.prototype = {
  init: function () {

    // this.stage.backgroundColor = '#FAFAFA';
    // this.stage.backgroundColor = '#d0f4f7';
    this.stage.backgroundColor = '#141414';
    
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.setMinMax(320, 480, 512, 768);
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;
    this.scale.setScreenSize(true);
    game.scale.refresh();

    this.game.renderer.renderSession.roundPixels = true;

  },
  create: function () {
    game.state.start('Preloader');
  }
};