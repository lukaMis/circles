console.log("Game log");

var PhaserGame = PhaserGame || {};


PhaserGame.Game = function (game) {

  this.player = null;
  this.playerTween = null;
  this.PLAYER_FIRE_MAX_WAIT = 6500;

  this.enemy = null;
  this.enemyInitialTween = null;
  this.enemyPatrolTween = null;

  this.currentScore = null;
  this.bestScore = null;

  // this.scoreText = null;

  this.enemySideMargin = 10;
  this.enemyTopMargin = 30 + this.enemySideMargin;

  this.stars = null;
  this.starTween = null;

  this.fireBar = null;
  this.fireBarTween = null;
  this.fireBarHeight = 8;

  this.distanceToCheck = null;

  console.log('FROM MAIN GAME FUNCTION');

  this.infoTextDOM = document.getElementById('info-text');
};


PhaserGame.Game.prototype.create = function () {

  console.log('CREATE FUNCTION');

  this.player = this.game.add.sprite(0, 0, 'player');
  this.player.anchor.setTo(0.5);

  this.enemy = this.game.add.sprite(0, 0, 'enemy');
  this.enemy.anchor.setTo(0.5);

  this.currentScore = 0;
  this.bestScore = localStorage.getItem('circleBestScore') === null ? 0:localStorage.getItem('circleBestScore');

  console.log('this.bestScore', this.bestScore);

  // this.scoreText = game.add.text(10, 10, "-", {
  //   font:"14px Arial",
  //   fill: "#ffffff"
  // });

  

  this.updateScore();

  this.gameInit();

  // go TO FIULL SCREEN LISTENER
  // game.input.onDown.add(this.goToFullScreen, this);

  this.distanceToCheck = this.player.width * 0.5 + this.enemy.width * 0.5;

  this.game.time.advancedTiming = true;
};

PhaserGame.Game.prototype.updateScore = function () {
  // this.scoreText.text = "Current: " + this.currentScore + " Best: " + this.bestScore;
  this.infoTextDOM.textContent = "Current: " + this.currentScore + " - " +" Best: " + this.bestScore;
}

PhaserGame.Game.prototype.gameInit = function () {
  this.createStars();
  this.showStar();
  this.positionEnemy();
  this.positionPlayer();
  this.drawFireBar();
}

PhaserGame.Game.prototype.createStars = function () {
  this.stars = game.add.group();
  this.stars.createMultiple(12, 'plus', 0, false);
  this.stars.setAll('anchor.x', 0.5);
  this.stars.setAll('anchor.y', 0.5);

  game.time.events.repeat(Phaser.Timer.SECOND * 2, 12, this.showStar, this);
}

PhaserGame.Game.prototype.showStar = function () {
  // this.stars
  var _star = this.stars.getFirstDead();
  _star.reset(game.world.randomX, game.world.randomY);
  _star.angle = 0;
  _star.alpha = 0.6;

  // var _newAngle = (Math.random() > 0.5) ? 60:0;
  // console.log(_newAngle);
  var _newAngle = 0;

  this.starTween = game.add.tween(_star).to( { y: '100', alpha: 0, angle: _newAngle }, 1500, "Linear", false);
  this.starTween.onComplete.add(this.killStar, this, _star);
  this.starTween.start();
  console.log('new star');
}

PhaserGame.Game.prototype.killStar = function (game, tween) {
  tween.target.kill();
}


PhaserGame.Game.prototype.positionEnemy = function () {

  // this.enemy.x = game.rnd.between(this.enemy.width * 0.5, this.game.width - this.enemy.width * 0.5);
  this.enemy.x = this.enemy.width * 0.5 + this.enemySideMargin;
  this.enemy.y = -this.enemy.height;
  
  var _startYPos = game.rnd.between( this.enemy.height * 0.5 + this.enemyTopMargin, this.game.height * 0.5 + this.enemyTopMargin);
  // var _startXPos = game.rnd.between(this.enemy.width * 0.5, this.game.width - this.enemy.width * 0.5);

  // this.enemyInitialTween = game.add.tween(this.enemy).to( { y: _startYPos }, 500, "Linear", false);
  this.enemyInitialTween = game.add.tween(this.enemy).to( { y: _startYPos }, 500, Phaser.Easing.Cubic.Out, false);
  this.enemyInitialTween.onComplete.add(this.startEnemyPatrol, this);
  this.enemyInitialTween.start();
};


PhaserGame.Game.prototype.startEnemyPatrol = function () {
  var _endPatrolPos = this.game.width - this.enemy.width * 0.5 - this.enemySideMargin;
  var _randomTweenTime = this.game.rnd.between(250, 1250);
  var _randomTweenDelay = this.game.rnd.between(0, 250);
  var _randomTweenYoyoDelay = this.game.rnd.between(0, 250);
  var _randomTween = Phaser.ArrayUtils.getRandomItem([
    'Linear', 
    Phaser.Easing.Cubic.In, 
    Phaser.Easing.Cubic.Out,
    Phaser.Easing.Cubic.InOut,
    
    // Phaser.Easing.Elastic.In, 
    // Phaser.Easing.Elastic.Out,
    // Phaser.Easing.Elastic.InOut
    // Phaser.Easing.Bounce.In, 
    // Phaser.Easing.Bounce.Out,
    // Phaser.Easing.Bounce.InOut,
  ]);

  this.enemyPatrolTween = game.add.tween(this.enemy).to( { x: _endPatrolPos }, _randomTweenTime, _randomTween, false, _randomTweenDelay);

  console.log('_randomTween:',_randomTween);
  console.log('_randomTweenTime:',_randomTweenTime);
  console.log('_randomTweenDelay:',_randomTweenDelay);
  console.log('_randomTweenYoyoDelay:',_randomTweenYoyoDelay);

  this.enemyPatrolTween.yoyo(true, _randomTweenYoyoDelay);
  this.enemyPatrolTween.repeat(-1, _randomTweenDelay);
  this.enemyPatrolTween.start();
};


PhaserGame.Game.prototype.positionPlayer = function () {

  // this.enemyInitialTween = game.add.tween(this.enemy).to( { y: _startYPos }, 500, Phaser.Easing.Cubic.Out, false);
  // this.enemyInitialTween.onComplete.add(this.startEnemyPatrol, this);
  // this.enemyInitialTween.start();

  this.player.x = this.world.width / 2;
  var _margin = this.player.height * 0.5;

  this.player.y = this.world.height + this.player.height;
  var _playerStartPosition = this.world.height - _margin * 3;

  this.player.angle = 0;
  this.player.playerOnTheMove = false;
  
  this.playerTween = game.add.tween(this.player).to( { y: _playerStartPosition }, 350, Phaser.Easing.Cubic.In, false);
  
  this.playerTween.onComplete.add(this.playerInStartPosition, this);
  this.playerTween.start();
};

PhaserGame.Game.prototype.playerInStartPosition = function () {
  this.playerFireTimer();
  this.addFireListener();
};

PhaserGame.Game.prototype.playerFireTimer = function () {

  this.animateFireBar();
  
  this.playerTween = game.add.tween(this.player).to( { y: this.game.height + this.player.height * 0.5 }, 500, Phaser.Easing.Elastic.In, false );

  this.playerTween.onComplete.add(this.playerDied, this);
  // this.playerTween.onStart.add(this.playerFireTimerTweenStarted, this);

  this.playerTween.delay(this.PLAYER_FIRE_MAX_WAIT);
  this.playerTween.start();
};

PhaserGame.Game.prototype.drawFireBar = function () {
  this.fireBar = game.add.graphics(0, 0);
  this.fireBar.lineStyle(0);
  this.fireBar.beginFill(0xFFFFFF);
  this.fireBar.drawRect(0, this.game.height - this.fireBarHeight, this.game.width, this.fireBarHeight);
  this.fireBar.endFill();

  // this.fireBar.opacity = 0;
};

PhaserGame.Game.prototype.animateFireBar = function () {
  // console.log('PhaserGame.Game.prototype.animateFireBar');
  // this.fireBar.width = this.game.width;
  // this.fireBar.opacity = 1;
  this.fireBarTween = game.add.tween(this.fireBar).to( { width: 0 }, this.PLAYER_FIRE_MAX_WAIT, 'Linear', true );
};

// PhaserGame.Game.prototype.drawAndAnimateFireBar = function () {
//   this.fireBar.lineStyle(0);
//   this.fireBar.beginFill(0xFFFFFF);
//   this.fireBar.drawRect(0, this.game.height-10, this.game.width, 10);
//   this.fireBar.endFill();
//   game.add.tween(this.fireBar).to( { width: 0 }, this.PLAYER_FIRE_MAX_WAIT, 'Linear', true );
// };

PhaserGame.Game.prototype.destroyFireBar = function () {
  this.fireBarTween.stop();
  
  // this.fireBar.width = 0;
  // this.fireBar.opacity = 0;
};



PhaserGame.Game.prototype.playerFireTimerTweenStarted = function () {
  this.player.playerOnTheMove = true;
};

PhaserGame.Game.prototype.addFireListener = function () {
  game.input.onDown.add(this.fire, this);
};

PhaserGame.Game.prototype.fire = function () {
  game.input.onDown.remove(this.fire, this);
  console.log('KABOOM');

  if(this.playerTween) {
    this.playerTween.stop();
    console.log('tween killed');
  }
  
  this.playerTween = game.add.tween(this.player).to( { y: -100 }, 1500, "Linear", false);
  this.playerTween.onComplete.add(this.playerDied, this);
  this.playerTween.start();

  this.player.playerOnTheMove = true;

  this.destroyFireBar();
};

PhaserGame.Game.prototype.playerDied = function () {
  localStorage.setItem('circleBestScore', Math.max(this.currentScore, this.bestScore) );
  game.state.start('Game');
};


PhaserGame.Game.prototype.update = function () {
  // console.log('Game works');

  if(this.player.playerOnTheMove) {
    // this.player.angle += 6;

    if(Phaser.Math.distance(this.player.x, this.player.y, this.enemy.x, this.enemy.y) < this.distanceToCheck ) {
      console.log('TOTAL HIT');
      this.collisionDetected();
    }
  }
};

PhaserGame.Game.prototype.render = function () {
  game.debug.text(game.time.fps + ' fps' || '--', this.game.width-40, this.game.height - 30, '#FFFFFF', '12px Arial' );
};

PhaserGame.Game.prototype.collisionDetected = function (context) {
  this.enemyPatrolTween.stop();
  this.playerTween.stop();
  this.player.playerOnTheMove = false;
  this.currentScore++;

  this.updateScore();
  this.gameInit();
};



PhaserGame.Game.prototype.goToFullScreen = function (context) {
  
  game.input.onDown.remove(this.goToFullScreen, this);
  game.scale.startFullScreen(false);
  game.input.onDown.add(this.goToNormalScreen, this); 
};


PhaserGame.Game.prototype.goToNormalScreen = function (context) {
  
  game.input.onDown.remove(this.goToNormalScreen, this);
  game.scale.stopFullScreen();
  game.input.onDown.add(this.goToFullScreen, this);
}