console.log("Preloader log");

var PhaserGame = PhaserGame || {};


PhaserGame.Preloader = function (game) {};


PhaserGame.Preloader.prototype.init = function () {

};

PhaserGame.Preloader.prototype.preload = function () {
  game.load.image('player', 'assets/images/player.png');
  game.load.image('enemy', 'assets/images/enemy.png');
  game.load.image('plus', 'assets/images/plus.png');
};


PhaserGame.Preloader.prototype.create = function () {
  game.state.start('MainMenu');
};